# Skand Frontend Task

Congratulations on reaching this stage of Skand's frontend/full stack developer recruitment process. In this stage, you will receive a series of tasks. The purpose and goals of this process is for you to:

1. Get you familiar with the basic tools Skand's dev team are using
2. Assert your skill in implementing the functionalities required by the team or clients
3. Assert your ability to learn new technologies as some of the tools required for this challenge may be new to you

## Task List

1. Deploy and host a React app on AWS
    - Use create-react-app to set up a React app
    - Research and document different ways to host your app on AWS
    - Choose one way and implement it
2. Set up Husky, Lint-staged, Prettier, ESLint and Commitizen in your project
    - Should include Prettier and ESLint in Lint-staged
    - Should run Lint-staged when git commit by Husky
    - Should run Commitizen when prepare git commit message by Husky
3. Set up CI/CD
    - Automatically build and deploy the React app to AWS when the branch merged into master
    - Feel free to use any CI/CD platform (ex. Bitbucket Pipeline, GitHub Actions, CircleCI, etc.)
4. Transfer your AWS infrastructure into [Pulumi](https://www.pulumi.com/) code
5. Implement a [Yeoman](https://yeoman.io/) generator to create the default React application, the Pulumi code and CI/CD configuration files.
    - Think about the reusability, parts of should be made configurable by Yeoman. For example, an easy one would be to be able to customize the name of the React app, but there are more e.g. the name of the infrastructure in Pulumi; we leave it up to you to surprise us

## The Deliverables

1. A link to your React app hosted on AWS
2. A link to a repository which includes
    1. Yeoman generator
    2. Pulumi code
    3. React application with linting and prettifying set up
    4. CI/CD configuration files

## Any questions?

Don't hesitate to ask! We're happy to guide you to succeed on this stage and soon become our potential team / family member.